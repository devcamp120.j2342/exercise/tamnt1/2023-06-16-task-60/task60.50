package com.devcamp.carapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.carapi.models.Car;

public interface CarRepository extends JpaRepository<Car, Integer> {
    @Query("SELECT c FROM Car c WHERE c.carCode = :carCode")
    Car getCarByCarCode(@Param("carCode") String carCode);
}
